﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Java.Lang;
using Java.Sql;
using Exception = System.Exception;
using Thread = System.Threading.Thread;

namespace AndroidApplication1
{
    [Activity(Label = "Java Reflector", MainLauncher = true, Icon = "@drawable/icon")]
    public class JavaReflector : Activity
    {
        private Button _InvokeButton;
        private Button _InterfaceInvokeButton;
        private Button _ConstructButton;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Main);
            _InterfaceInvokeButton = FindViewById<Button>(Resource.Id.interface_invoke);
            _InvokeButton = FindViewById<Button>(Resource.Id.invoke);
            _ConstructButton = FindViewById<Button>(Resource.Id.construct);

            _ConstructButton.Click += delegate
            {
                var t = Task.Factory.StartNew(() => ExploreTypes());
                t.ContinueWith(ConstructTypes);
            };
            _InvokeButton.Click += delegate
            {
                var t = Task.Factory.StartNew(() => ExploreTypes());
                t.ContinueWith(InvokeEachMethod);
            };
            _InterfaceInvokeButton.Click += delegate
            {
                var t = Task.Factory.StartNew(() => ExploreTypes());
                t.ContinueWith(InterfaceInvokeEachMethod);
            };
        }

        private void ReekHavok()
        {
            for (; ; )
            {
                var tasks = new List<Task>();
                for (var i = 0; i < new Random().Next(16); ++i)
                {
                    var t = Task.Factory.StartNew(() =>
                    {
                    });
                    tasks.Add(t);

                }
                Task.WaitAll(tasks.ToArray());
                //wait for the threads to go away
                Thread.Sleep(1000);
            }
        }
        static readonly Type[] NO_ARGS_TYPES = new Type[] { };
        static readonly object[] NO_ARGS = new object[] { };


        //use a seed so that we can get a specific run to happen again. 0 is alphabetical
        private const int TEST_SEED = 0;

        private void Log(string format, params object[] args)
        {
            return;
            if (args.Length >= 0)
                format = string.Format(format, args);

            Console.WriteLine(format);    
        }

        private ICollection<Type> ExploreTypes()
        {
            Log("Enumerating Types");
            var types = new LinkedList<Type>();
                
            var jlo = typeof (Java.Lang.Object);
            foreach (var type in jlo.Assembly.GetTypes())
            {
                if (type.GetConstructor(NO_ARGS_TYPES) == null)
                    continue;
                if (type.IsAbstract)
                    continue;
                //we dont want to call take, etc
                if (type.FullName.StartsWith("Java.Util.Concurrent"))
                    continue;
                //skip all the android specific to avoid any stuff that wants to be on the ui thread
                if (type.FullName.StartsWith("Android.OS"))
                    continue;
                //we dont want to call read, etc
                if (type.FullName.StartsWith("Java.IO"))
                    continue;
                //lets not mess with the java gc
                if (type.FullName.StartsWith("Java.Lang.Ref.ReferenceQueue"))
                    continue;
                //lets not mess with activity stack
                if (type.FullName.StartsWith("Android.App.Instrumentation"))
                    continue;
                //vm aborts
                if (type.FullName.StartsWith("Android.OS.Debug"))
                    continue;
                //skip lang cause it has weird methods
                if (type.FullName.StartsWith("Java.Lang"))
                    continue;
                //crash
                if (type.FullName.StartsWith("Android.Media.MediaRecorder"))
                    continue;
                //crash
                if (type.FullName.StartsWith("Android.Media.SmsMessage"))
                    continue;
                //crash
                if (type.FullName.StartsWith("Android.Opengl"))
                    continue;
                //crash
                if (type.FullName.StartsWith("Android.Graphics.Camera"))
                    continue;

                if (!ON_UI_THREAD)
                {
                    //so we can run on a bg thread without much worry
                    if (type.FullName.StartsWith("Android"))
                        continue;
                }

                types.AddLast(type);
                Log("Type: {0}", type.FullName);
            }
            Log("Completed Construction");

            if (TEST_SEED == 0)
                return types;
            var random = new Random(TEST_SEED);
            return types.OrderBy(i => random.Next()).ToList();
        }

        //we can run these on the ui thread to avoid as many thread restrictions as possible
        //its a lot slower, so it off, and we aren't running android classes
        private const bool ON_UI_THREAD = false;
        private ManualResetEvent _Waiter = new ManualResetEvent(false);

        private void RunUnit(Action a)
        {
            if (!ON_UI_THREAD)
            {
                Task.Factory.StartNew(a);
                return;
            }

            lock (this)
            {
                RunOnUiThread(() =>
                {
                    a();
                    _Waiter.Set();
                });
                _Waiter.WaitOne();
                _Waiter.Reset();
            }
        }

        private void ConstructTypes(Task<ICollection<Type>> types)
        {
            Log("Creating Instances");
            foreach (var type in types.Result)
            {
                RunUnit(() =>
                {
                    Log("Creating: {0}", type.FullName);
                    var constructor = type.GetConstructor(NO_ARGS_TYPES);
                    try
                    {
                        var o = constructor.Invoke(NO_ARGS);
                    }
                    catch (Exception e)
                    {
                        
                    }
                });
            }
            Log("Completed Construction");
        }
        private void InvokeEachMethod(Task<ICollection<Type>> types)
        {
            Log("Invoke Method");
            foreach (var type in types.Result)
            {
                InvokeAllEasyMethods(type, type.FullName);       
            }
            Log("Completed Invocation");
        }
        private void InterfaceInvokeEachMethod(Task<ICollection<Type>> types)
        {
            Log("Invoking via interface");
            foreach (var type in types.Result)
            {
                foreach (var inter in type.GetInterfaces())
                {
                    InvokeAllEasyMethods(inter, string.Format("{0}:{1}", type.FullName, inter.FullName));                        
                }
            }
            Log("Completed Invocation");
        }

        private void InvokeAllEasyMethods(Type inter, string info)
        {
            foreach (var method in inter.GetMethods(BindingFlags.Public | BindingFlags.Instance))
            {
                RunUnit(() =>
                {
                    //don't call methods on an object after disposing it
                    if (method.Name == "Dispose")
                        return;
                    Log("Creating: {0}", inter.FullName);
                    var constructor = inter.GetConstructor(NO_ARGS_TYPES);
                    object o;
                    try
                    {
                        o = constructor.Invoke(NO_ARGS);
                    }
                    catch (Exception e)
                    {
                        return;
                    }
                    Log("Constructing params: {0} -> {1}", info, method.Name);
                    var pis = method.GetParameters();
                    var p = new object[method.GetParameters().Length];
                    try
                    {
                        var i = 0;
                        foreach (var pi in pis)
                        {
                            var pc = pi.ParameterType.GetConstructor(NO_ARGS_TYPES);
                            if (pc == null)
                                throw new Exception("Not available");
                            if (pc.IsAbstract)
                                throw new Exception("Not available");
                            p[i++] = pc.Invoke(NO_ARGS);
                        }
                    }
                    catch (Exception e)
                    {
                        return;
                    }

                    Log("Invoking: {0} -> {1}", info, method.Name);
                    try
                    {
                        method.Invoke(o, p);
                    }
                    catch (Exception e)
                    {
                    }
                });
            }
        }
    }
}

